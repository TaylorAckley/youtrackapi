"use strict";
const express = require('express');
const app = express();
var server = require('http').createServer(app);
const morgan = require('morgan');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const path = require('path');
const colors = require('colors');
const cors = require('cors');
const async = require('async');
const moment = require('moment');

app.use(morgan('dev')); // log with Morgan
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.text());
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
}));
app.use(methodOverride());
app.use(cors());


//app deps
const appConfig = require('./config.js');
const helpers = require('./modules/module.helpers.js');
const youtrack = require('./modules/module.youtrack.js');
const routes = require('./routes/slack.routes.js')(app);
const bot = require('./modules/bot.youtrack.js');

bot.start();

console.log(appConfig);

server.listen(appConfig.port, function() {
    console.log('Server listening at port %d', appConfig.port);
});