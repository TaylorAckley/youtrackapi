"use strict";

const moment = require('moment');
const req = require('request');
const config = require('./../config.js');
const Promise = require('bluebird');
const xml = require('xml2js')
const _ = require('lodash');
const youtrack = require('./../modules/module.youtrack.js');


module.exports = {
    start: function() {
        const RtmClient = require('@slack/client').RtmClient;
        const CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;
        var RTM_EVENTS = require('@slack/client').RTM_EVENTS;
        const token = config.SLACK_YT_TOKEN;
        let rtm = new RtmClient(token, {
            logLevel: 'debug'
        });
        rtm.start();
        rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, function(rtmStartData) {
            console.log(`Logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}, but not yet connected to a channel`);
        });
        rtm.on(RTM_EVENTS.MESSAGE, function(message) {
            // Listens to all `message` events from the team
            console.log(message);
            let isYT = config.YT_REGEX.test(message.text);
            if (isYT) {
            	message.text.replace(config.YT_REGEX, function(val) {
            		console.log(val);
            		return '<https://knowany.myjetbrains.com/youtrack/issue/' + val + '|' + val + '>';
            	});
            	console.log(message);
                /*
                  rtm.updateMessage(message, function (err, res) {
    				if (err) {
    					console.log(err);
    				}
    				console.log(res);
  				}); */

                        req.post('https://slack.com/api/chat.update?token=' + config.SLACK_YT_TOKEN + '&channel=' + message.channel + '&as_user=true&attachments=' + JSON.stringify(issue),
                                function(err, response, body) {
                                    if (err) {
                                        console.log(err);
                                    }
                                });



                let ytIssue = message.text.match(config.YT_REGEX);
                _.each(ytIssue, function(v) {
                let ytIssueNum1 = v.replace('{{', '');
                	let ytIssueNum2 = ytIssueNum1.replace('}}', '');
                	youtrack.getIssue(ytIssueNum2)
                    	.then(function(issue) {
                        	req.post('https://slack.com/api/chat.postMessage?token=' + config.SLACK_YT_TOKEN + '&channel=' + message.channel + '&as_user=true&attachments=' + JSON.stringify(issue),
                            	function(err, response, body) {
                                	if (err) {
                                    	console.log(err);
                                	}
                            	});
                    		}).catch(function(err) {
                       	 		console.log(err);
                    		});

                })
            }
        });
    }
};