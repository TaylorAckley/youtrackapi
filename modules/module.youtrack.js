"use strict";

const moment = require('moment');
const req = require('request');
const config = require('./../config.js');
let Promise = require('bluebird');
const xml = require('xml2js');

//knowany.myjetbrains.com/youtrack/rest/user/login?login=support@knowledgeanywhere.com&password=kn0w!!5

module.exports = {
    authYT: function() {
        return new Promise(function(resolve, reject) {
            req.post(config.YOUTRACK_URL + '/rest/user/login?login=' + config.YOUTRACK_LOGIN + '&password=' + config.YOUTRACK_PASSWORD, function(err, res, body) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                resolve({
                    headers: res.headers,
                    body: body
                });
            });
        });

    },
    getIssue: function(number) {
        return new Promise(function(resolve, reject) {
            module.exports.authYT()
                .then(function(res) {
                    req.get({
                        url: config.YOUTRACK_URL + '/rest/issue/' + number,
                        headers: {
                            'Cookie': res.headers['set-cookie']
                        }
                    }, function(error, response, body) {
                        if (error) {
                            console.log(error);
                            reject(error);
                        }

                        xml.parseString(body, function(err, issue) {
                            if (err) {
                                reject(err);
                            }
                            console.log(typeof(issue.error));
                            console.log(issue.error);
                            if (issue.error) {
                            	console.log('Im here');
                            	reject(issue.error);
                            }

                            if (!issue.error) {
                                var assigned;
                                if (!issue.issue.field[18].value[0].$.fullName) {
                                     assigned = 'Unassigned'; 
                                } else {
                                    assigned = issue.issue.field[18].value[0].$.fullName;
                                }


                            let responseObj = [{
                                    "title": issue.issue.$.id + '-' + issue.issue.field[2].value,
                                    "text": issue.issue.field[3].value[0],
                                    "title_link": 'http://knowany.myjetbrains.com/youtrack/issue/' + issue.issue.$.id,
                                    "fields": [{
                                        "title": "Created",
                                        "value": moment.unix(issue.issue.field[4].value).format('MMMM Do YYYY, h:mm:ss a'),
                                        "short": true

                                    }, {
                                        "title": "Updated",
                                        "value": moment.unix(issue.issue.field[5].value).format('MMMM Do YYYY, h:mm:ss a'),
                                        "short": true

                                    }, {
                                        "title": "Type",
                                        "value": issue.issue.field[13].value[0],
                                        "short": true

                                    }, {
                                        "title": "State",
                                        "value": issue.issue.field[14].value[0],
                                        "short": true

                                    }, {
                                        "title": "Priority",
                                        "value": issue.issue.field[16].value[0],
                                        "short": true

                                    }, {
                                        "title": "Assigned",
                                        "value": assigned,
                                        "short": true

                                    }]
                                }
                                //,{"text": issue.issue}
                            ];
                            resolve(responseObj);
                        }
                        });
                    });
                });
        });

    },
    createIssue: function(project, summary, description) {
        return new Promise(function(resolve, reject) {

            module.exports.authYT()
                .then(function(res) {
                    let summaryClean = summary.substring(1, summary.length - 1);
                    let descriptionClean = description.substring(1, description.length - 1);
                    req.put({
                        url: config.YOUTRACK_URL + '/rest/issue?project=' + project + '&summary=' + encodeURI(summaryClean) + '&description=' + encodeURI(descriptionClean),
                        headers: {
                            'Cookie': res.headers['set-cookie']
                        }
                    }, function(error, response, body) {
                        if (error) {
                            console.log(error);
                            reject(error);
                        }
                        resolve(response.headers.location.replace('rest/', ''));

                    });
                });
        });
    }
};