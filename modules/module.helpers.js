"use strict";

const moment = require('moment');
const req = require('request');
const config = require('./../config.js');
const Promise = require('bluebird');
const xml = require('xml2js')


module.exports = {
    replaceWhiteSpace: function(str) {
        return str.replace(/([^"]+)|("[^"]+")/g, function($0, $1, $2) {
            if ($1) {
                return $1.replace(/\s/g, '');
            } else {
                return $2;
            }
        });
    }
}