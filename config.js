"use strict";

// For these environment variables to be propogated, you must set a .env file for your local machine.
// Any new environment variables must be added to heroku.

var config = {
    env: process.env.ENV,
    port: process.env.PORT,
    API_URL: process.env.API_URL,
    SLACK_TOKEN: process.env.SLACK_TOKEN,
    SLACK_URL: process.env.SLACK_URL,
    SLACK_YT_TOKEN: process.env.SLACK_YT_TOKEN,
    SLACK_CLIENT: process.env.SLACK_CLIENT,
    SLACK_SECRET: process.env.SLACK_SECRET,
    YOUTRACK_URL: process.env.YOUTRACK_URL,
    YOUTRACK_LOGIN: process.env.YOUTRACK_LOGIN,
    YOUTRACK_PASSWORD: process.env.YOUTRACK_PASSWORD,
    YT_REGEX: /([a-zA-Z]){3,16}\w+\-([0-9)]){0,5}\d+/g,
    YT_REGEX_BRACKETS: /({)([a-zA-Z]){3,16}\w+\-([0-9)]){0,5}\d+(})/g
  };



module.exports = config;
