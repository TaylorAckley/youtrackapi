"use strict";
const config = require('./../config.js');
const youtrack = require('./../modules/module.youtrack.js');
const helpers = require('./../modules/module.helpers.js');

module.exports = function(app) {

    app.post('/api/slack/command', function(req, res) {
        let args = helpers.replaceWhiteSpace(req.body.text);
        let argsSplit = args.split(',')
        switch (argsSplit[0]) {
            case 'get':
                youtrack.getIssue(argsSplit[1])
                    .then(function(issue) {
                        res.status(200).send({
                            "response_type": "in_channel",
                            //"text": 'Retrieving details for ' + issue.issue.$.id + '-' + issue.issue.field[2].value,
                            "attachments": issue
                        });
                    });
                break;
            case 'add':
                youtrack.createIssue(argsSplit[1], argsSplit[2], argsSplit[3])
                    .then(function(issue) {
                        res.status(200).send({
                            "response_type": "in_channel",
                            "text": 'New Issue Created',
                            "attachments": [{
                                    "text": issue
                                }
                                //,{"text": issue.issue}
                            ]
                        });
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
                break;
            default:
                res.status(200).send({
                    "response_type": "in_channel",
                    "text": 'Command not recognized',
                    "attachments": [{
                            "text": 'Commands: \n /youtrack get, [issue number]  ex: /youtrack get, SIE06001-2399 \n /youtrack add, [project], "[summary with quotes]", [description with quotes]   ex: /youtrack add, IMC12001, "My Summary", "My Description"'
                        }
                        //,{"text": issue.issue}
                    ]
                });
        }


    });
};